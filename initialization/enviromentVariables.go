package initialization

import (
	"log"
	"net/url"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

func LoadEnviromentVariables() (botApiKey string, chatId int64, interval int, itemUrl *url.URL) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	botApiKey = os.Getenv("ZR_TELEGRAM_API_KEY")

	chatId, err = strconv.ParseInt(os.Getenv("ZR_TELEGRAM_CHATID"), 10, 64)
	if err != nil {
		panic(err)
	}

	interval, err = strconv.Atoi(os.Getenv("ZR_INTERVAL_SECONDS"))
	if err != nil {
		panic(err)
	}

	itemUrl, err = url.ParseRequestURI(os.Getenv("ZR_URL"))
	if err != nil {
		panic(err)
	}

	return
}
