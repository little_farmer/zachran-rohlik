package main

import (
	"zachran-rohlik/frontend"
	"zachran-rohlik/initialization"
	"zachran-rohlik/scrapper"

	"fmt"
	"time"
)

func main() {
	// Loading env variables
	botApiKey, chatId, interval, itemUrl := initialization.LoadEnviromentVariables()

	// setup telegram bot
	bot := frontend.LoadTelegramBot(botApiKey)

	// setup ticker
	ticker := time.NewTicker(time.Duration(interval) * time.Second)
	defer ticker.Stop()

	// initilize status on startup of application
	available, priceText, err := scrapper.ScrapeRohlik(itemUrl.String())
	previousAvailable := available

	fmt.Printf("Started\nItem available: %v\nPrice: %v\nItem URL: %v", available, priceText, itemUrl)

	// forever loop with ticker and checking for change in availability
	for {
		select {
		case <-ticker.C:
			available, priceText, err = scrapper.ScrapeRohlik(itemUrl.String())
			if err != nil {
				fmt.Println(err)
				continue
			}

			if previousAvailable != available {
				frontend.SendChangeToTelegram(bot, chatId, available, priceText, itemUrl.String())
				previousAvailable = available
			}
		}
	}
}
