package scrapper

import (
	"errors"
	"fmt"
	"strings"

	"github.com/gocolly/colly"
)

func ScrapeRohlik(url string) (available bool, priceText string, err error) {
	c := colly.NewCollector(colly.AllowedDomains("www.rohlik.cz"))

	c.OnResponse(func(r *colly.Response) {
		if r.StatusCode != 200 {
			errMessage := fmt.Sprintf("Status code for response is: %v", r.StatusCode)
			err = errors.New(errMessage)
		}
	})

	// find SPAN for sold out items
	c.OnHTML("span.productDetail__soldOut", func(h *colly.HTMLElement) {
		priceText = strings.TrimSpace(h.Text)
	})

	// find current price on items, only when they are in stock
	c.OnHTML("div.currentPrice", func(h *colly.HTMLElement) {
		priceText = strings.TrimSpace(h.Text)
	})

	c.Visit(url)

	// set availability
	if priceText == "Momentálně vyprodáno" {
		available = false
	} else {
		available = true
	}

	return
}
