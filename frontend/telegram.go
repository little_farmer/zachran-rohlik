package frontend

import (
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func LoadTelegramBot(botApiKey string) (bot *tgbotapi.BotAPI) {
	// setup telegram bot
	bot, err := tgbotapi.NewBotAPI(botApiKey)
	if err != nil {
		fmt.Println(err)
	}
	return
}

func SendChangeToTelegram(bot *tgbotapi.BotAPI, chatID int64, available bool, priceText string, url string) {
	var message string

	// make HTML string for text body of message
	if available {
		message = "Dostupne"
	} else {
		message = "Nedostupne"
	}
	message = fmt.Sprintf("<b>%v</b>\n<b>%v</b>\n%v", message, priceText, url)

	// create message
	msg := tgbotapi.NewMessage(chatID, message)
	msg.ParseMode = "HTML"

	// send message
	_, err := bot.Send(msg)
	if err != nil {
		panic(err)
	}
}
